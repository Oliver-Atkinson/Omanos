#!/usr/bin/env python
"""Loads an image for a given region and time, runs analyses as requested and saves images to Google Drive"""

##############################################################################################################################
# Expansions:
#   Maybe find a way to choose the best (i.e. most recently launched/best resolution/most data) satellite with available data 
#   More satellites
#   Maybe overhaul visualisations s.t. they're read in in im_saver, and passed straight to the visualisation - allows for removal of the empty palette for RGB and extra options when needed
#   Dicts of analyses types, e.g. fire detection, vegetation health, water stuff, etc.?
#   Maybe build up some kind of masking library? Something like a default snow mask that will always apply some threshold on NDSI?
##############################################################################################################################

#To do image wide calculations, use e.g. im.select(band).reduceRegion(ee.Reducer.sum().unweighted(),aoi,res).get(band)

#To dos
#Options for time sampling in collection getting
#Include some nice progress prints
#Change detection - likely involved
#Mixed point and area analysis
# More customisability in CollectionGetter, e.g cloudy percentage, application of cloud mask


#Setting up for GEE
import ee
ee.Initialize()
# ee.Authenticate() #Not sure how often this needs to be run
# exit()

#Other imports
import numpy as np
import logging
import yaml
import sys
from functools import partial
from IndexFunctions import *
from QuantFunctions import *
from VisFunctions import *
from CollectionLoading import *
# import timeit


logger = logging.getLogger(__name__)


# print('New: ', timeit.timeit('func()', globals=locals(), number = 50)/50)

#################################

#Identifying the run card to use from the command line input
RunCard = sys.argv[1]

#################################


# Handy global variables

#Lists of indices where multiple are calculated at once
#Have to expand these everytime a new analysis is added - a little clunky
NDIs_lib = ['NDVI','NDWI','NDMI','NDSI','NDRE','GNDVI']#,'NBR' - no NBR vis yet, fix post change detection...
Ratm1s_lib = ['RECI','GCI']
SAVIs_lib = ['SAVI','OSAVI','MSAVI']
RGB_lib = ['RGB', 'RedVegetation1','RedVegetation2', 'SWIRComposite1', 'Agriculture', 'SWIRComposite2','Bathymetric1','Bathymetric2','Bathymetric3','Bathymetric4','Bathymetric5','Bathymetric6','Urban']
RGBCalc_lib = ['Neon']
Misc_lib = ['SIPI','EVI','VARI','ARVI', 'ARI', 'MARI', 'PSRI', 'OSI', 'BSI']
Index_lib =  NDIs_lib+Ratm1s_lib+SAVIs_lib+Misc_lib+['SAR_trial']
SAR_RGB_lib = ['SAR_RGB'] 
Analysis_lib = Index_lib+RGB_lib+RGBCalc_lib+SAR_RGB_lib

#List of quantative analyses
Quant_lib = ['AreaMean', 'AreaStdDev', 'AreaThreshold', 'PointValue']

#List of available operations for reducing image collections to single images
Reducers = ['median', 'mosaic']

#Possible analyses for each satellite
# S2_analyses = ['NDVI','NDWI','NDMI','NDSI','GNDVI','RECI','GCI','SAVI','OSAVI','MSAVI','SIPI','EVI','VARI','ARVI']
# L9_analyses = ['NDVI','NDWI','NDMI','NDSI','GNDVI','RECI','GCI','SAVI','OSAVI','MSAVI','SIPI','EVI','VARI','ARVI']
# L8_analyses, L7_analyses, L5_analyses, L4_analyses = L9_analyses, L9_analyses, L9_analyses, L9_analyses

#Allowed time units in Earth Engine (ignoring hour, minute and second)
time_units = ['year','Year', 'month','Month', 'week','Week', 'day','Day']
plural_time = [unit+'s' for unit in time_units]
time_units += plural_time

#################################

#Change these to just be one list of min max

#Defining areas of interest from the geojson site ([[long_min, lat_min], [long_max, lat_max]])
aoi_mirf = ee.Geometry.Rectangle([[-1.7726, 53.6308], [-1.6078, 53.7000]]) #Mirfield, England
aoi_glas = ee.Geometry.Rectangle([[-4.39, 55.82], [-4.22, 55.92]]) #Glasgow, Scotland
aoi_boum = ee.Geometry.Rectangle([[3.3155, 36.6850], [3.6025, 36.8162]])   #Boumerdes, Algeria
aoi_LNG = ee.Geometry.Rectangle([[28.0537, 60.4277], [28.1667, 60.6091]])  #Portovaya LNG Terminal, Russia
aoi_kini = ee.Geometry.Rectangle([[-9.4052, 11.5772], [-9.3546, 11.6220]]) #Kintinian, Guinea
aoi_zina = ee.Geometry.Rectangle([[7.5291, 46.0318], [7.8192, 46.1701]]) #Bishorn, Switzerland
# aoi_high = ee.Geometry.Rectangle([[-5.3671, 57.6733], [-5.134, 57.8629]])  #Scottish highlands
aoi_high = ee.Geometry.Rectangle([[-3.8644, 57.0112], [-3.5465, 57.121]])  #Scottish highlands
aoi_test = ee.Geometry.Rectangle([[-5, 54.25], [-2.5, 57]]) #Glasgow, Scotland

aoi_stir = ee.Geometry.Rectangle([[-4.072, 55.974], [-3.477, 56.1704]]) #Stirling, Scotland
aoi_nz = ee.Geometry.Rectangle([[172.5826, -43.921],[173.239, -43.573]]) #Lake Ellesmere, New Zealand
aoi_sund = ee.Geometry.Rectangle([[88.366, 21.537],[89.981, 22.406]]) #Sunderban, Bangladesh
aoi_gc = ee.Geometry.Rectangle([[-114.094, 35.731],[-113.0479, 36.261]]) #Grand Canyon, Arizona
aoi_Hong = ee.Geometry.Rectangle([[113.7579, 22.134], [114.5146, 22.5356]]) #Hong Kong, China


############################################
# aoi, aoi_name = aoi_mirf, "mirf"
# aoi, aoi_name = aoi_glas, "glas"
# aoi, aoi_name = aoi_boum, "Boum"
# aoi, aoi_name = aoi_LNG, "LNG-2021"
# aoi, aoi_name = aoi_kini, "kini"
# aoi, aoi_name = aoi_test, "test"
# aoi, aoi_name = aoi_zina, 'Zinal'
# aoi, aoi_name = aoi_high, 'High'

# aoi, aoi_name = aoi_stir, 'Stir'
# aoi, aoi_name = aoi_nz, 'NZ'
# aoi, aoi_name = aoi_sund, 'Sund'
# aoi, aoi_name = aoi_gc, 'GC'
# aoi, aoi_name = aoi_Hong, 'Hong'

vis_aois = [aoi_stir, aoi_nz, aoi_gc, aoi_boum, aoi_kini, aoi_mirf]



#Check that the requested satellite is known
# if sat not in Satellite_Information.keys(): raise ValueError(str(sat)+" not recognised, currently supported satellites are "+str(list(Satellite_Information.keys())))

############################################

#Annoyingly, taking a mosaic or a median of a collection loses useful information that we would like to keep
#So, defining a couple of quick functions to rectify this

def mosaicer(col: ee.ImageCollection):
    """Takes a mosaic over a collection, but retains important properties, such as satellite and image dates,
    from the original collection."""
    #Get the original properties
    props = col.getInfo()['properties']
    #Take a mosaic using the in-built Earth Engine method, and imprint the properties on it
    col_mos = col.mosaic().set({'Satellite': props['Satellite'],
                'StartDate': props['StartDate'],
                'EndDate': props['EndDate'],
                'Resolution': props['Resolution']})
    return col_mos

def medianer(col: ee.ImageCollection):
    """Takes a median over a collection, but retains important properties, such as satellite and image dates,
    from the original collection."""
    #Get the original properties
    props = col.getInfo()['properties']
    #Take a median using the in-built Earth Engine method, and imprint the properties on it
    col_mos = col.median().set({'Satellite': props['Satellite'],
                'StartDate': props['StartDate'],
                'EndDate': props['EndDate'],
                'Resolution': props['Resolution']})
    return col_mos

def im_saver(im: ee.Image, analysis: str, aoi: ee.Geometry, vis: dict):
    """Saves an image to a Google Drive file.
    
    First, the filename is set, using the properties of the image to be saved and the bands chosen, with the format of
    Satellite_Bands_StartDate_EndDate. After exception handling to check that the inputs are valid, a visualisation of the
    chosen bands is made, and passed to the Earth Engine batch export method to save to Drive.
    
    Parameters:
    im: ee.Image
        The image from which to extract bands to create a visualisation, which is then saved to file
    analysis: str
        The name of the analysis which will be used to create an image - can either be a single band or three
        bands, in which case they will be saved as RGB
    aoi: ee.Geometry
        The physical area, covered by the image, that should be saved to file
    vis: dict
        Dictionary containg the minimum and maximum values (floats or ints) to be used in creating the visualisation, as well
        as the colour palette to be used (list of hex strings) for single band images
    """

    #Retrieiving the relevant image information
    im_props = im.getInfo()['properties']
    #Analysis type checking
    if type(analysis) is not str and type(analysis) is not list: raise TypeError("The analysis to be saved must an analysis name (string) or list of bands.")

    #Create a bespoke filename based on the satellite, analysis and date range
    if type(analysis) == list:
        filename = ''
        for band in analysis: 
            filename += band
        filename += "_"+im_props['Satellite'][0]+im_props['Satellite'][-1]+"_"+im_props['StartDate']+"_"+im_props['EndDate']
    else: filename = analysis+"_"+im_props['Satellite'][0]+im_props['Satellite'][-1]+"_"+im_props['StartDate']+"_"+im_props['EndDate']
    filename = aoi_name+"_"+filename 
    #Could check if the file already exists?

    #Translating the analysis to a list of strings correspinding to the bands to save    
    if analysis in RGB_lib or analysis in RGBCalc_lib or analysis in SAR_RGB_lib: bands = [analysis+'_R', analysis+'_G', analysis+'_B']
    elif type(analysis) == list: bands = analysis
    else: bands = [analysis]

    #Exception handling, otherwise code may run fine but not save the image - any other ways the image can fail?
    #Too many or too few bands chosen
    if len(bands) < 1 or len(bands) > 3: raise ValueError("Invalid number of bands ("+str(len(bands))+"), must chose 1-3 bands.")
    #Ensure that the image is not empty and all chosen bands are present in the image
    im_bands = im.bandNames().getInfo()
    if len(im_bands) == 0: raise ValueError("Attempted to save an empty image: "+filename)
    for band in bands: 
        if band not in im_bands: raise ValueError("Band "+band+" not in image. Valid bands are: "+str(im.bandNames().getInfo()))
    
    #Values from the visualisation dictionary
    vis_min, vis_max, palette = vis['vis_min'], vis['vis_max'], vis['palette']

    #Min and max for the visualisation are invalid - may be made redundant here by handling everything through the VisLoader function, check...
    if vis_min >= vis_max: raise ValueError("Minimum value greater than the maximum for the visualisation (vis_min >= vis_max)")
    #Palette is uneccessarily supplied for an 2 or 3 band image, left empty or is only a single colour for a single band image
    if len(bands) != 1 and len(palette) != 0: raise ValueError("Palettes can only be specified for single band images.")
    if len(bands) == 1 and len(palette) == 1: raise ValueError("Palettes must have 2 or more colours.")
    if len(bands) == 1 and len(palette) == 0: logger.warning("Warning: No palette specified for a single band ("+bands[0]+") image. Greyscale will be used.")

    #Creating a visualisation to save to file
    #For single band images use a given palette, if not default to greyscale (palette arg can only be given for single band images)
    if len(bands) == 1: im_save = im.visualize(**{'bands': bands, 'min': vis_min, 'max': vis_max, 'palette': palette}) 
    else: im_save = im.visualize(**{'bands': bands, 'min': vis_min, 'max': vis_max}) 
    
    #Extract the resolution to be used - may be a source of unexpected issues in certain cases (bands of different res for the same sat)
    res = im_props['Resolution']

    # print(filename)
    # if analysis != 'RGB':
    #     min_max= im.select([analysis]).reduceRegion(ee.Reducer.minMax(),aoi,res).getInfo()
    #     print(bands, min_max)
    #     print(ee.Number(im.select([analysis]).reduceRegion(ee.Reducer.count(),aoi,res).get(analysis)).getInfo())

    #Annoyingly, this method does not allow for subfolders to be used
    #The task which saves the image to Drive
    task = ee.batch.Export.image.toDrive(**{
    'image': im_save,                        #Image to be exported
    'description': filename,                 #File name
    'folder': ImSaveDir,                     #Export folder
    'scale': res,                            #Spatial resolution 
    'region': aoi                            #Region to cover
    })

    #Running the task
    task.start()

    return

def AnalysisRunner(im: ee.Image, band_dict: dict, analyses: list):
    """Takes an image and runs the requested analyses on it
    
    By making use of the functions collected in IndexFunctions, calculates a given list of indices and adds each of these
    as a band to the original image.
    
    Parameters:
    im: ee.Image
        The image for which the indices are to be calculated
    band_info: dict
        The dictionary of satellite bands that translates names to band IDs (e.g. R -> B4), as given in Satellites.yml
    analyses: list
        The list of analyses to be performed, as strings

    Returns:
    ee.Image
        The original image, with an additional band for each index calculated
    """

    #Assumes checking on analysis list has been done - see the analyser function for how

    #Build lists of indices to pass to functions that calculate multiple indices
    NDIs = [ind for ind in analyses if ind in NDIs_lib]
    Ratm1s = [ind for ind in analyses if ind in Ratm1s_lib]
    SAVIs = [ind for ind in analyses if ind in SAVIs_lib]
    Composites = [comp for comp in analyses if comp in RGB_lib]

    #Creating an image to add the indices bands to
    im_plus_ind = im

    #Do the requested analyses - must be added to everytime a new analysis is added - clunky, fix...
    if len(NDIs) != 0: im_plus_ind = NDI_calc(im_plus_ind, band_dict, NDIs)
    if len(Ratm1s) != 0: im_plus_ind = ratm1_calc(im_plus_ind, band_dict, Ratm1s)
    if 'SIPI' in analyses: im_plus_ind = SIPI_calc(im_plus_ind, band_dict)
    if 'EVI' in analyses: im_plus_ind = EVI_calc(im_plus_ind, band_dict)
    if len(SAVIs) != 0: im_plus_ind = SAVIs_calc(im_plus_ind, band_dict, SAVIs)
    if 'ARVI' in analyses: im_plus_ind = ARVI_calc(im_plus_ind, band_dict)
    if 'VARI' in analyses: im_plus_ind = VARI_calc(im_plus_ind, band_dict)
    if 'ARI' in analyses: im_plus_ind = ARI_calc(im_plus_ind, band_dict)
    if 'MARI' in analyses: im_plus_ind = MARI_calc(im_plus_ind, band_dict)
    if 'BSI' in analyses: im_plus_ind = BSI_calc(im_plus_ind, band_dict)
    if 'PSRI' in analyses: im_plus_ind = PSRI_calc(im_plus_ind, band_dict)
    if 'OSI' in analyses: im_plus_ind = OSI_calc(im_plus_ind, band_dict)
    if len(Composites) != 0: im_plus_ind = RGBComposite(im_plus_ind, band_dict, Composites)
    if 'Neon' in analyses: im_plus_ind = Neon(im_plus_ind, band_dict)
    if 'SAR_RGB' in analyses: im_plus_ind = SAR_RGB(im_plus_ind, band_dict)
    if 'SAR_trial' in analyses: im_plus_ind = SAR_trial(im_plus_ind, band_dict)
    
    return im_plus_ind

# Maybe just return the mask so that it can then be applied to multiple images?
def ImageMasker(aoi: ee.Geometry, im: ee.Image, mask_bandName: str, upper_bound: float = 1e5, lower_bound: float = -1e5, save_raster=0, save_vector=0):
    """Applies a mask to an image from a upper and/or lower bounds on values in a given band
    
    By selecting the given band of an image, ideally an index, and setting upper and lower bounds on this index's value
    across the image, a mask is generated and applied, such that the output image is only shown for pixels that satisfy the
    bounding criteria. The mask itself can also optionally be saved as a seperate image.
    
    Parameters:
    aoi: ee.Geometry
        The physical area, covered by the image
    im: ee.Image
        The image from which to calculate, and then apply, a mask
    mask_bandName: str
        The name of the band which is used to calculate the mask
    upper_bound: float (Optional)
        The upper bound for the values in mask_bandName; values greater than this will be masked. N.B. An upper or lower bound must be passed
    lower_bound: float (Optional)
        The lower bound for the values in mask_bandName; values less than this will be masked. N.B. An upper or lower bound must be passed
    save_raster: int [0,1] (Optional)
        Flag (0/1 for off/on) - if on saves the mask itself as a raster image (.tif)
    save_vector: int [0,1] (Optional)
        Flag (0/1 for off/on) - if on saves the mask itself as a vector image (.shp)

    Returns:
    ee.Image
        The original image, with the mask applied to all bands."""
    
    #Exception handling - anything else here?
    if type(lower_bound) is not int and type(lower_bound) is not float: raise TypeError('Lower bound of the mask must be int or float.')
    if type(upper_bound) is not int and type(upper_bound) is not float: raise TypeError('Upper bound of the mask must be int or float.')
    if upper_bound == 1e5 and lower_bound == -1e-5: raise ValueError('Either the lower or upper bound must be set to non-default values.')
    if lower_bound >= upper_bound: raise ValueError("Lower bound of the mask must be below the upper bound.")

    #Getting some of the image properties 
    im_bands = im.bandNames().getInfo()
    im_props = im.getInfo()['properties']
    res = im_props['Resolution']

    #If the requested mask band is not in the image, check to see if the analysis can be calculated, if not raise an error
    if mask_bandName not in im_bands:
        if mask_bandName in Index_lib:
            im = AnalysisRunner(im, Satellite_Information[im_props['Satellite']]['Bands'], [mask_bandName])
        else: raise ValueError('Requested band for mask generation, '+mask_bandName+' is neither in the image ('+im_bands+') nor a valid index ('+Index_lib+')')

    mask_band = im.select([mask_bandName])

    #Creating the mask
    mask_upper = mask_band.lte(upper_bound)
    mask_lower = mask_band.gte(lower_bound)
    comb_mask = mask_lower.add(mask_upper).subtract(1) #The combined mask will have values of 1 or 2; subtract 1 to shift to 0 or 1 as befits a mask

    #Check that the mask doesn't kill too much of the image
    N_pix = ee.Number(im.select(mask_bandName).reduceRegion(ee.Reducer.count(),aoi,res).get(mask_bandName))
    N_gmask = ee.Number(comb_mask.select(mask_bandName).reduceRegion(ee.Reducer.sum().unweighted(),aoi,res).get(mask_bandName))
    g_perc = N_gmask.divide(N_pix).multiply(100).getInfo()
    #Maybe include a print statement here as well, just to say what proportion of the image passes masking
    #Value of the cut offs are somewhat arbitary, and may not always be an issue, but worth having it in as a warning
    if g_perc <= 5: logger.warning('Warning: A low proportion ('+"{0:.3g}".format(g_perc)+'%) of the image passes the masking criteria. Images may not save correctly.')
    elif g_perc >= 95: logger.warning('Warning: A high proportion ('+"{0:.3g}".format(g_perc)+'%) of the image passes the masking criteria; the mask may be ineffectual.')

    #Actually apply the mask
    masked_im = im.updateMask(comb_mask)

    #If the relevant flag is switched on, save the mask, with black for masked pixels and white unmasked
    if save_raster == 1: 
        mask_save = comb_mask.visualize(**{'bands': mask_bandName, 'min': 0, 'max': 1}) 

        #Essentially just ripping out the image saving part of im_saver, with the mask as the image to visualise
        #Including the mask bounds in the filename as well, by checking if they match the defaults
        if lower_bound != -1e5 and upper_bound == 1e5: bound_str = 'gte_'+str(lower_bound)
        if upper_bound != 1e5 and lower_bound == -1e5: bound_str = 'lte_'+str(upper_bound)
        if lower_bound != -1e5 and upper_bound != 1e5: bound_str = 'gte_'+str(lower_bound)+'_lte_'+str(upper_bound)
        filename = aoi_name+'_'+mask_bandName+'_'+bound_str+'_Mask'+'_'+im_props['Satellite'][0]+im_props['Satellite'][-1]+"_"+im_props['StartDate']+"_"+im_props['EndDate']

        #The task which saves the image to Drive
        task = ee.batch.Export.image.toDrive(**{
        'image': mask_save,                       #Image to be exported
        'description': filename,                  #File name 
        'folder': MaskSaveDir,                    #Export folder 
        'scale': res,                             #Spatial resolution 
        'region': aoi                             #Region to cover
        })
    
        #Running the task
        task.start()

        #Saving as a shape file (vector) if requested
        if save_vector == 1:
            #Pretty much just taken from Clare's code, this exports the mask as a vector image
            #It works - not sure why things have to be the way they are, but it works
            vector = comb_mask.addBands(im.select([mask_bandName])).reduceToVectors(**{
                'geometry': ee.FeatureCollection(aoi), #Geometry just has to be a feature collection
                'crs': im.projection(),
                'scale': res,
                'geometryType': 'polygon',
                'eightConnected': False,
                'labelProperty': 'zone',
                'reducer': ee.Reducer.mean(),
            })

            task = ee.batch.Export.table.toDrive(
                collection = vector,
                description = filename, 
                folder = MaskSaveDir,
                fileFormat = 'SHP'
                )
            task.start()

    return masked_im

#Mask loading function - seems to work fairly well, if not perfectly
def MaskLoader(aoi: ee.Geometry, mask_name: str, invert: int=0, overlap_len: int=2):
    """Loads a mask from a file uploaded to Google Earth Engine.
    
    The mechanics of this function are a little unclear and the invert and overlap_len values require tuning on a file by file 
    basis, without much apparent pattern of why they should take the required values. 
    
    Parameters:
    im: ee.Image
        The image on which to apply the mask
    aoi: ee.Geometry
       The area of interest, which should span the image
    mask_name: str
        The name of the mask asset as it appears in Google Earth Engine
    invert: int [0,1]
        Flag (0/1 for off/on) to signal if the loaded mask requires inversion
    overlap_len: int [1,2] 
        The length of overlapping geometries to be clipped from the mask, should be 1 or 2

    Returns:
    ee.Image
        The mask as an Earth Engine image
    """

    #Load the mask file as a feature collection
    fc = ee.FeatureCollection(mask_name)

    #Turn the feature collection into an image
    mask = ee.Image.constant(1).clip(fc.geometry()).mask().Not()
    #Get the coordinates of the geometry of this image - must be a better way to get this list from the feature collection, check...
    coord_list = mask.getInfo()['properties']['system:footprint']['coordinates']
    #The longest coordinate list has to be removed (presumably it borders the whole aoi)
    coord_list.remove(max(coord_list, key=len))

    #Picking out multiple coordinate lists to allow for overlapping areas
    #The length should be one (returning the original list) or two, depending on unknown factors in the aoi
    overlap_areas = [ee.Geometry.Polygon(coords) for coords in coord_list if len(coords) >= overlap_len]

    #Turning the coordinate lists into a feature collections
    feature_list = [ee.Feature(ee.Geometry.Polygon(coord_list[i]), {'system:index': str(i)}) for i in range(len(coord_list))]
    feature_col = ee.FeatureCollection(feature_list)

    overlap_list = [ee.Feature(overlap_areas[i], {'system:index': str(i)}) for i in range(len(overlap_areas))]
    overlap_col = ee.FeatureCollection(overlap_list)

    #Set up base images to use
    roi_image = ee.Image(0).clip(aoi)
    loaded_image = ee.Image(1).clipToCollection(feature_col)
    #Using the 'where' method to create a first mask, then clipping to the overlaps and repeating the 'where' method
    binary_image = roi_image.where(loaded_image, loaded_image)
    overlap_image = binary_image.clipToCollection(overlap_col)
    binary_overlap = roi_image.where(overlap_image, overlap_image)

    #If the inversion flag is required, do this
    if invert == 1: binary_overlap = binary_overlap.eq(0)

    #Return the resulting mask image
    return binary_overlap

#Similarly to the masking and analyses, create some common library, e.g. healthy veg from NDVI?
def QuantAnalysis(im_list: list, areas: list, bandName: str, analyses: list, upper_bound: float = 1e5, lower_bound: float = -1e5, save_flag=1):
    """Collects the functionality for quantatative analysis over region(s) or point(s), such as mean, with plotting and illustrative image saving.
    
    The image list is iterated over to create a single array that stores all the requested analyses as a time series, with a sub array for 
    each requested analysis, which should each correspond to a predefined function. The plots for each individual region/point are then saved,
    along with a combined plot if multiple regions/points are passed to the function. The regions/points can also be highlighted and saved onto an RGB
    image if the relevant flag is switched on.

    Parameters:
    im_list: list
        A list of images, in chronological order (oldest first), in which the areas sit
    areas: list
        A list of points or regions, as ee.Point or ee.Geometry respectively, on which to perform the analysis
    bandName: str
        The name of the band on which to perform the analysis
    analyses: list
        A list of strings of analysis names, each of which should relate to a pre-defined function 
    upper_bound: float (Optional)
        Upper bound for threshold analyses
    lower_bound: float (Optional)
        Lower bound for threshold analyses
    save_flag: int [0,1] (Optional)
        Flag (0/1 for off/on) - if on saves the most recent image with the regions/points painted on it
    """

    #Exception handling - plenty of this
    #Check that if the threshold is requested then at least one of the bounds is non-default, implement...
    #Check each of the areas passed is valid - need to check that each is within the image aoi, implement...
    
    #Getting some of the image properties - assumes first image is representative, fix...
    im_bands = im_list[0].bandNames().getInfo()
    im_props = im_list[0].getInfo()['properties']

    #If the requested mask band is not in the image, check to see if the analysis can be calculated, if not raise an error
    if bandName not in im_bands:
        if bandName in Index_lib: im_list = [AnalysisRunner(im, Satellite_Information[im_props['Satellite']]['Bands'], [bandName]) for im in im_list]
        else: raise ValueError('Requested band for quantatative analysis, '+bandName+' is neither in the image ('+str(im_bands)+') nor a valid index ('+str(Index_lib)+')')

    #Analysis list not a list - could maybe move this elsewhere once this analysis chain is folded into the overall stuff
    if type(analyses) is not list: raise TypeError("Analysis list not a list.")
    #No analyses requested 
    if len(analyses) == 0: raise ValueError("No analyses requested - empty list passed to QuantAnalysis.")
    #Remove duplicate indices in the user list
    analyses = list(set(analyses))
    #Checking that all requested analyses exist - bit clunky 
    bad_ans = [an for an in analyses if an not in Quant_lib]
    if len(bad_ans) != 0: logger.warning("Warning: Unrecognised analyses requested (and ignored): "+str(bad_ans)+". Valid analyses are "+str(Quant_lib))
    good_ans = [an for an in analyses if an not in bad_ans]

    #Bounds and flag checks
    if type(lower_bound) is not int and type(lower_bound) is not float: raise TypeError('Lower bound of the mask must be int or float.')
    if type(upper_bound) is not int and type(upper_bound) is not float: raise TypeError('Upper bound of the mask must be int or float.')
    if lower_bound >= upper_bound: raise ValueError("Lower bound of the mask must be below the upper bound.")
    if save_flag != 0 and save_flag != 1: raise ValueError("Invalid flag value for region saving in the quantatative analysis function - only 0 and 1 accepted.")

    #Setting a name for the regions to be used in the legend, assumes all points or all areas
    if areas[0].getInfo()['type'] == 'Polygon': name = 'Area'
    else: name = 'Point'

    #Only doing point or area analyses as appropriate
    good_ans = [an for an in good_ans if name not in an]

    #This gets used quite a bit
    n_areas = len(areas)

    #Initalising structures to hold the data
    data, s_dates, e_dates = np.zeros((len(good_ans), n_areas, len(im_list))), [],[]
    #For each image, get the resolution and dates, then perform the requested analyses
    for i in range(len(im_list)):
        im = im_list[i]
        im_props = im.getInfo()['properties']
        res = im_props['Resolution']
        s_dates.append(im_props['StartDate'])
        e_dates.append(im_props['EndDate'])

        plotband = im.select([bandName])

        for j in range(n_areas):
            area = areas[j]
            for k in range(len(good_ans)):
                #Threshold analyses require upper and lower bounds as additional args
                if good_ans[k] == 'AreaThreshold': data[k,j,i] = AreaThreshold(plotband, area, res, bandName, upper_bound, lower_bound)
                else: data[k,j,i] = globals()[good_ans[k]](plotband, area, res, bandName)

    #Individual plots
    for l in range(n_areas):
        for m in range(len(good_ans)):  
            analysis = good_ans[m]

            #Maybe remove if the s.d. is ever something we'd want to see seperately...
            if analysis == 'AreaStdDev': continue

            #Set a filename
            filename = aoi_name+'_'+analysis+str(l+1)+'_'+bandName+'_'+im_props['Satellite'][0]+im_props['Satellite'][-1]+"_"+s_dates[0]+'-'+e_dates[-1]

            #For plotting the mean with s.d. as an error bar
            if analysis == 'AreaMean' and 'AreaStdDev' in analyses: TimePlotter(s_dates, data[m,l,:], bandName, PlotDir+"/"+filename, errs=data[analyses.index('AreaStdDev'),l,:])
            
            #If a threshold analysis is called the filename should additionally reflect the bounds
            elif analysis == 'AreaThreshold': 
                if lower_bound != -1e5 and upper_bound == 1e5: bound_str = 'gte_'+str(lower_bound)
                if upper_bound != 1e5 and lower_bound == -1e5: bound_str = 'lte_'+str(upper_bound)
                if lower_bound != -1e5 and upper_bound != 1e5: bound_str = 'gte_'+str(lower_bound)+'_lte_'+str(upper_bound)
                filename = aoi_name+'_'+analysis+str(l+1)+'_'+bandName+'_'+bound_str+'_'+im_props['Satellite'][0]+im_props['Satellite'][-1]+"_"+s_dates[0]+'-'+e_dates[-1]
                TimePlotter(s_dates, data[m,l,:], 'Percentage', PlotDir+"/"+filename)

            #Normal plotting otherwise
            else: TimePlotter(s_dates, data[m,l,:], bandName, PlotDir+"/"+filename)

    #Combined plot if multiple regions requested
    if n_areas > 1:
        for n in range(len(good_ans)):
            analysis = good_ans[n]
            filename = aoi_name+'_Comb'+analysis+'_'+bandName+'_'+im_props['Satellite'][0]+im_props['Satellite'][-1]+"_"+s_dates[0]+'-'+e_dates[-1]

            #Same caveats on the plotting as above
            if analysis == 'AreaStdDev': continue
            if analysis == 'AreaMean' and 'AreaStdDev' in analyses: TimePlotter(s_dates, data[n,:,:], bandName, PlotDir+"/"+filename, name=name, errs=data[analyses.index('AreaStdDev'),:,:])
            elif analysis == 'AreaThreshold': 
                filename = aoi_name+'_Comb'+analysis+'_'+bandName+'_'+bound_str+'_'+im_props['Satellite'][0]+im_props['Satellite'][-1]+"_"+s_dates[0]+'-'+e_dates[-1]
                TimePlotter(s_dates, data[n,:,:], 'Percentage', PlotDir+"/"+filename, name=name)
            else: TimePlotter(s_dates, data[n,:,:], bandName, PlotDir+"/"+filename, name=name)

    #Painting the regions onto the image if requested
    # Would be better if the points were clearer to see, playing with colours etc, fix...
    if save_flag == 1:
        implusvis = im
        #Slightly different painting procedures for points and areas
        if name == 'Point':
            for i in range(n_areas):
                implusvis = implusvis.paint(ee.FeatureCollection(areas[i].buffer(20)), ee.Number(0.5), ee.Number(20)) #Play with changing the width here
        elif name == 'Area':
            for i in range(n_areas):
                implusvis = implusvis.paint(ee.FeatureCollection(areas[i]), ee.Number(1), ee.Number(10))
        implusvis = implusvis.set({'Satellite': sat,
                    'StartDate': s_dates[-1].format('YYYY-MM-dd'),
                    'EndDate': e_dates[-1].format('YYYY-MM-dd'),
                    'Resolution': res})
        #If RGB not in the image, change this
        if 'RGB_R' not in implusvis.bandNames().getInfo(): implusvis = AnalysisRunner(implusvis, Satellite_Information[sat]['Bands'], ['RGB'])
        im_saver(implusvis, 'RGB', aoi, VisLoader('RGB_default', 'RGB'))

    return

def Analyser(aoi: ee.Geometry, sat: str, process_dict: dict, dates_dict: dict, mask_dict: dict, analyses_dict: dict, vis_dict: dict = {}):
    """Performs the requested analyses; getting collections and saving images along the way.
    
    Firstly, the global area of interest and date checks are performed. Runs one of the time collection generators, and creates
    a dictionary of partial functions, one for each satellite present in the collections. Iterates over the list of collections,
    reduces each of these to a single image, then performs the requested analysis and saving.

    Parameters:
    aoi: ee.Geometry
        The area of interest 
    sat: str
        The name of the satellite to use data from
   process_dict: dict
        A dictionary with the image processing parameters
            reducer: str - The name of the operation (currently median or mosaic) used to reduce collections to a single image
            cloud_cover: float - The value by which to filter for images with less than the given cloud coverage percentage
            cloud_mask: int [0,1] - Flag for application of the cloud mask
    dates_dict: dict
        A dictionary containing all the relevant date information:
            start_date: ee.Date - Start of the overall period
            end_date: ee.Date - End of the overall period
            delta_period: float (Optional) - Number of time units to step between sub-periods, e.g. sample every delta_period days/months/years, defaults to 1  
            period: str (Optional)  - The time unit to step between sub-periods, e.g. every n period, defaults to year
            delta_sub: float - Length of sub-period time to sample, in units of subperiod, e.g. a duration of delta_sub days/months/years  
            subperiod: str - Time unit for the sub-period, e.g. delta_sub subperiods
            sub_start: ee.Date - Start of the sub-period to sample (must be within the overall start and end dates), defaults to the overall start_date
    mask_dict: dict
        A dictionary containing all the relevant masking information: 
            mask_bandName: string - The name of the band which is used to calculate the mask
            upper_bound: float - Upper bound of the mask
            lower_bound: float - Lower bound of the mask
            save_raster: int - Flag (0/1 for off/on) - if on saves the mask itself as a raster image (.tif)
            save_vector: int - Flag (0/1 for off/on) - if on saves the mask itself as a vector image (.shp)
            load_name: string - Filepath for the Earth Engine asset to load as a mask
            load_invert: int [0,1] - Flag for inversion of a loaded mask
            load_overlap: int [1,2] - Length of overlao regions to clip from a loaded mask
    analyses_dict: dict
        A dictionary of the information for quantative analyses:
            reducer: string - The name of the operation to use to reduce individual collections to single images
            analyses: list - The list of indices to be calculated, as strings - includes RGB composites
            areas: list - A list of Earth Engine geometries to analyse
            bandName: string - The name of the band to perform the analysis on
            quant_analyses: list - A list of strings of analysis names, each of which should relate to a pre-defined function
            upper_bound: float - Upper bound for threshold analyses
            lower_bound: float - Lower bound for threshold analyses
            save_flag: int [0,1] - Flag (0/1 for off/on) - if on saves the most recent image with the regions/points painted on it
    vis_dict: dict
        Dictionary of visualisation dictionaries; format of {index: {vis_min: value, vis_max: value, palette: string or list of hex strings}}
    """

    #Exception handling - much of this takes place in the dedicated functions that this function serves to run
    #More complete exception handling here may make that in the individual functions redundant, check...    

    #Get a list of [Collection, Satellite] pairings, using the existence of the delta sub period as a de facto flag
    if dates_dict['delta_sub'] is not None: Col_list = SubperiodSeries(aoi, sat, process_dict, dates_dict['start_date'], dates_dict['end_date'], dates_dict['delta_sub'], dates_dict['subperiod'], dates_dict['sub_start'], dates_dict['delta_period'], dates_dict['period'])
    else: Col_list = TimeSeries(aoi, sat, process_dict, dates_dict['start_date'], dates_dict['end_date'], dates_dict['delta_period'], dates_dict['period'])
    
    #This is a bit awkward, but not horrendous - while the analysis functions can be used to map over collections, the im_saver cannot (client side requirements)
    
    #Splitting out the collection and satellite lists and reducing collections to single images
    red_list = [globals()[process_dict['reducer']](ColPair[0]) for ColPair in Col_list] 
    sat_list = [ColPair[1] for ColPair in Col_list]
    #Getting useful variables from these
    unique_sats = list(set(sat_list))
    n_ims, n_sats = len(red_list), len(unique_sats)

    #Creating a dictionary of partial functions, one for each satellite
    AnalysisPartials = [partial(AnalysisRunner, band_dict=Satellite_Information[u_s]['Bands'], analyses=analyses_dict['indices']) for u_s in unique_sats]
    PartialDict = dict(zip(unique_sats, AnalysisPartials))

    #Masking options - include a check that both loading and generation aren't both on (maybe only a warning as this could be useful)
    mask_band, mask_upper, mask_lower, mask_raster, mask_vector = mask_dict['mask_bandName'], mask_dict['upper_bound'], mask_dict['lower_bound'], mask_dict['save_raster'], mask_dict['save_vector']
    mask_file, mask_invert, mask_overlap = mask_dict['load_name'], mask_dict['load_invert'], mask_dict['load_overlap']
    if type(mask_file) is str: mask = MaskLoader(aoi, mask_file, mask_invert, mask_overlap)

    im_savers = {}
    #Creating a dictionary of image saving functions, one for each index
    for analysis in analyses_dict['indices']:
        #Getting the visualisation - if none specified, look up the default settings
        if analysis not in list(vis_dict.keys()): vis = Visualisations[analysis+'_default']
        else: vis = vis_dict[analysis]
        #Add the partial im_saver to the dictionary
        im_savers.update({analysis: partial(im_saver, analysis=analysis, aoi=aoi, vis=VisLoader(vis,analysis))})

    #Do the required masking, if any
    masked_ims = []
    for im in red_list:
        if type(mask_band) is str: im = ImageMasker(aoi, im, mask_band, mask_upper, mask_lower, mask_raster, mask_vector)
        if type(mask_file) is str: im = im.updateMask(mask)
        masked_ims.append(im)

    #For each satellite, use the list of masked and reduced images for the analyses with the partial functions
    for i in range(n_sats):
        col_sat = unique_sats[i]
        Analysed_list = [PartialDict[col_sat](masked_ims[j]) for j in range(n_ims) if sat_list[j] == col_sat]
        #Iterate over each image in the satellite list, saving each analysis requested
        for im in Analysed_list:
            for analysis in analyses_dict['indices']:
                im_savers[analysis](im)

    #Do the quantative analyses on the full selection of images - this badly needs testing for multiple sat collections and times (e.g. L7, S2, L7), resolve...
    if len(analyses_dict['quant_analyses']) != 0: 
        #Doing points and areas in seperate runs if both types of geometry are requested
        if type(analyses_dict['areas'][0]) is list:
            QuantAnalysis(masked_ims, analyses_dict['areas'][0], analyses_dict['bandName'], analyses_dict['quant_analyses'], analyses_dict['upper_bound'], analyses_dict['lower_bound'], analyses_dict['save_flag'])
            QuantAnalysis(masked_ims, analyses_dict['areas'][1], analyses_dict['bandName'], analyses_dict['quant_analyses'], analyses_dict['upper_bound'], analyses_dict['lower_bound'], analyses_dict['save_flag'])
        else: QuantAnalysis(masked_ims, analyses_dict['areas'], analyses_dict['bandName'], analyses_dict['quant_analyses'], analyses_dict['upper_bound'], analyses_dict['lower_bound'], analyses_dict['save_flag'])

    return

#Open up the runcard file and extract all the relevant information, with plenty of input checking done 
def RunCardReader(Runcard: str):
    #Opening up the given run card and extracting the full data
    with open(Runcard, "r") as stream:
        try: data = yaml.safe_load(stream)
        except yaml.YAMLError as exc: raise ValueError(exc)

    #Getting the satellite out
    sat = data['Satellite']
    if type(sat) is not str: raise TypeError('Satellite name must be a string.')
    if sat not in Satellite_Information.keys(): raise ValueError(str(sat)+" not recognised, currently supported satellites are "+str(list(Satellite_Information.keys())))

    #Splitting out the dictionaries to individual ones
    names, process, dates, masking, quants, vis = data['Names'], data['Processing'], data['Dates'], data['Masking'], data['Analyses'], data['Visualisations']
    
    #Type and valid character check on the names
    def NameChecker(filename, name):
        if type(filename) is not str: raise TypeError(name+' name must be a string.')
        safe_name = re.sub(r'[^\w_. -]', '', filename)
        if safe_name != filename: logger.warning('Warning: Unsafe characters in '+name+' name. Renamed to '+safe_name)
        return safe_name  
    names['Name'], names['ImSaveDir'],names['MaskSaveDir'], names['PlotDir'] = NameChecker(names['Name'], 'AOI'), NameChecker(names['ImSaveDir'], 'Image saving folder'), NameChecker(names['MaskSaveDir'], 'Mask saving folder'), NameChecker(names['PlotDir'], 'Plot saving folder')

    #Exception handling function for all flags - if not included, set to off
    def FlagChecker(flag, name):
        if flag is None: flag = 0
        if flag != 0 and flag != 1: raise ValueError("Invalid value for "+name+" flag - only 0 and 1 accepted.")
        return flag

    #Checking if numbers are indeed valid numbers (and converting to floats as required)
    def NumChecker(value, name):
        if value is None: return #May be an issue if some required numbers are not included in the run card
        try: value = float(value)
        except ValueError: raise TypeError(name+' must be a number.')
        return value

    #Image processing 
    if process['reducer'].lower() not in Reducers: raise ValueError("Requested reducer not recognised. Valid reducers are "+str(Reducers))
    process['reducer'] = process['reducer'].lower()+'er'
    # Cloud perc must be number between 0 and 100, if None set to 100
    if process['cloud_cover'] is None: process['cloud_cover'] = 100
    process['cloud_cover'] = NumChecker(process['cloud_cover'], 'Maximum cloud cover percentage')
    if process['cloud_cover'] > 100 or process['cloud_cover'] < 0: raise ValueError('Maximum cloud cover percentage must be between 0 and 100.')
    process['cloud_mask'] = FlagChecker(process['cloud_mask'], 'Cloud mask')
    
    #Checking validity of the coordinates
    def CoordChecker(value, name):
        NumChecker(value, name)
        if 'long' in name:
            if abs(value) > 180: raise ValueError('Longitudes must be between -180 and 180 degrees; given value was '+str(value))
        if 'lat' in name:
            if abs(value) > 90: raise ValueError('Latitudes must be between -90 and 90 degrees; given value was '+str(value))
        return
    for coord in ['min_long', 'min_lat', 'max_long', 'max_lat']:
        CoordChecker(data[coord], coord)

    #Changing the aoi coordinates to an EE geometry - assume rectangular area
    aoi = ee.Geometry.Rectangle(data['min_long'], data['min_lat'], data['max_long'], data['max_lat'])
    #Global AOI checker
    AoiChecker(sat, aoi)

    #Date dictionary; converting to Earth Engine date objects and error handling
    dates['start_date'], dates['end_date'] = ee.Date(dates['start_date']), ee.Date(dates['end_date']) #Start and end date checks are handled by the DateChecker function, run in Analyser
    #Exception handling for the subperiod cases
    if dates['delta_sub'] is not None:
        if dates['sub_start'] is None: dates['sub_start'] = 0 #Set default sub start 
        if dates['subperiod'] not in time_units: raise ValueError('Sub-period must be a valid time unit.')
    if dates['sub_start'] is not None:
        dates['sub_start'] = ee.Date(dates['sub_start'])
        try: dates['sub_start'].getInfo()
        except ee.ee_exception.EEException: raise ValueError("Invalid sub period start date format; use YYYY-MM-dd.")
    dates['delta_sub'], dates['delta_period'] = NumChecker(dates['delta_sub'], 'Delta sub-period'), NumChecker(dates['delta_period'], 'Delta period')
    #Set to defaults if nothing entered
    if dates['delta_period'] is None: dates['delta_period'] = 1
    if dates['period'] is None: dates['delta_period'] = 'year'
    #Check valid time units
    if dates['period'] not in time_units: raise ValueError('Period must be a valid time unit.')

    #Global date checker
    # DateChecker(sat, dates['start_date'], dates['end_date'])

    #Masking exception handling - some handled in the functions themselves
    masking['load_invert'], masking['save_raster'], masking['save_vector'] = FlagChecker(masking['load_invert'], 'mask loading inversion'), FlagChecker(masking['save_raster'], 'mask raster saving'), FlagChecker(masking['save_vector'], 'mask vector saving') 
    #Float conversions to allow for scientific notation read in 
    masking['upper_bound'], masking['lower_bound'] = NumChecker(masking['upper_bound'], "Mask upper bound"), NumChecker(masking['lower_bound'], "Mask lower bound")
    if masking['load_overlap'] != 1 and masking['load_overlap'] != 2: raise ValueError("Overlap length in the mask loading must be set to 1 or 2.")

    #Analyses exception handling

    #Index and quantatative analysis list handling - covered mainly in the functions themselves so only type handling here
    if type(quants['indices']) is str:
        inds = quants['indices'].strip().split()
        inds = [re.sub(',', '', ind) for ind in inds]
        quants['indices'] = inds
    if type(quants['quant_analyses']) is str:
        quans = quants['quant_analyses'].strip().split()
        quans = [re.sub(',', '', quan) for quan in quans]
    #Area checks
    if quants['areas'] is not None:
        quant_areas = [ee.Geometry.Rectangle(coords) for coords in quants['areas'] if len(coords) > 2]
        quant_points = [ee.Geometry.Point(coords) for coords in quants['areas'] if len(coords) == 2]
        areas = quant_areas+quant_points
        #Check that all these areas/points are valid
        for area in areas:
            #Check that the areas are formatted correctly
            if area.getInfo()['type'] != 'Polygon' and area.getInfo()['type'] != 'Point': raise TypeError("Region for quantatative analysis not formatted as a valid area or point.")
            #Check that the area is bounded
            if area.isUnbounded().getInfo() == True: raise ValueError("Region for quantatative analysis is unbounded.")
            #Check that the areas sit within the AOI
            if aoi.contains(area).getInfo() == False: raise ValueError("Region for quantatative analysis is outside the overall AOI.") 
        quants.update({'areas': [quant_areas, quant_points]})
    # Bound checks
    quants['upper_bound'], quants['lower_bound'] = NumChecker(quants['upper_bound'], "Threshold analysis upper bound"), NumChecker(quants['lower_bound'], "Threshold analysis lower bound")

    # Type checks
    if quants['indices'] is None: quants['indices'] = []
    if quants['quant_analyses'] is None: quants['quant_analyses'] = []
    if type(quants['indices']) is not list: raise TypeError("Indices list is not a list.")
    if type(quants['quant_analyses']) is not list: raise TypeError("Quantatative analyses list is not a list.")
    if type(quants['bandName']) is not str and type(quants['bandName']) is not None: raise TypeError('Band name for quantatative analyses must be a string.')
    #No analyses requested 
    if len(quants['indices']+quants['quant_analyses']) == 0: raise ValueError("No indices or analyses requested.")

    #Remove duplicate indices in the user list
    quants['indices'] = list(set(quants['indices']))
    #Checking that all requested analyses exist - bit clunky 
    bad_ans = [an for an in quants['indices'] if an not in Analysis_lib]
    if len(bad_ans) != 0: logger.warning("Warning: Unrecognised analyses requested (and ignored): "+str(bad_ans)+". Valid analyses are "+str(Analysis_lib))
    quants['indices'] = [an for an in quants['indices'] if an not in bad_ans]

    #Should compatibility handling be included here, in case of both making and defined vis?
    # Empty visualisation input handling - only defaults will be used in this case
    if vis is None: vis = {}
    # Checking for any custom visualisations and including these in the dictionary
    for ind in vis.keys():
        ind_vis = vis[ind]
        if ind_vis['make'] == 1:
            if ind_vis['floor'] is None: ind_vis.update({'floor': '#000000'})
            if ind_vis['ceiling'] is None: ind_vis.update({'ceiling': '#FFFFFF'})
            if ind_vis['n_steps'] is None: ind_vis.update({'n_steps': 10})
            cust_vis = VisMaker(ind_vis['vals'], ind_vis['colours'], ind_vis['steps_flag'], ind_vis['ramp_flag'], ind_vis['show'], ind+'_Vis', ind_vis['floor'], ind_vis['ceiling'], ind_vis['n_steps'])
            vis.update({ind: cust_vis})
        else:
            ind_vis['vis_min'], ind_vis['vis_max'], ind_vis['palette'] = NumChecker(ind_vis['vis_min'], 'Visualisation minimum'), NumChecker(ind_vis['vis_max'], 'Visualisation maximum'), ColourListChecker(ind_vis['palette'])
            vis.update({ind: ind_vis})

    return names, aoi, sat, process, dates, masking, quants, vis

#Read all the information in
names, aoi, sat, process_dict, dates_dict, mask_dict, analyses_dict, vis_dict = RunCardReader(RunCard)
#Setting useful global variables
aoi_name, ImSaveDir, MaskSaveDir, PlotDir = names['Name'], names['ImSaveDir'], names['MaskSaveDir'], names['PlotDir']
#Finally, doing the analysis
Analyser(aoi, sat, process_dict, dates_dict, mask_dict, analyses_dict, vis_dict)