\contentsline {section}{\numberline {1}Introduction}{2}{section.1}%
\contentsline {section}{\numberline {2}Background Information}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}Fundamentals of Orbital Dynamics}{2}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Basics of Earth Observations}{3}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Types of Satellite Imaging}{4}{subsection.2.3}%
\contentsline {subsubsection}{\numberline {2.3.1}Multi-spectral Imaging}{5}{subsubsection.2.3.1}%
\contentsline {subsection}{\numberline {2.4}An Introduction to Google Earth Engine}{6}{subsection.2.4}%
\contentsline {section}{\numberline {3}A Brief EO Satellite Taxonomy}{6}{section.3}%
\contentsline {subsection}{\numberline {3.1}Landsat}{6}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}MODIS}{7}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Sentinel 1}{7}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Sentinel 2}{7}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}Planet Labs SkySat}{7}{subsection.3.5}%
\contentsline {section}{\numberline {4}Installation and Setup}{8}{section.4}%
\contentsline {section}{\numberline {5}Structure}{9}{section.5}%
\contentsline {section}{\numberline {6}Usage}{10}{section.6}%
\contentsline {subsection}{\numberline {6.1}Run card inputs}{10}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}Dates}{11}{subsection.6.2}%
\contentsline {subsection}{\numberline {6.3}Masking}{12}{subsection.6.3}%
\contentsline {subsection}{\numberline {6.4}Analytic information}{12}{subsection.6.4}%
\contentsline {subsection}{\numberline {6.5}Visualisations}{13}{subsection.6.5}%
\contentsline {section}{\numberline {7}Expansions}{14}{section.7}%
\contentsline {subsection}{\numberline {7.1}Adding an index}{14}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Adding a visualisation}{14}{subsection.7.2}%
\contentsline {subsection}{\numberline {7.3}Adding a quantitative analysis}{14}{subsection.7.3}%
\contentsline {subsection}{\numberline {7.4}Adding a satellite}{15}{subsection.7.4}%
\contentsline {section}{\numberline {8}Developer Notes}{15}{section.8}%
\contentsline {section}{\numberline {9}Index of Indices}{16}{section.9}%
\contentsline {subsection}{\numberline {9.1}Normalised Difference Index}{16}{subsection.9.1}%
\contentsline {subsubsection}{\numberline {9.1.1}Vegetation}{16}{subsubsection.9.1.1}%
\contentsline {subsubsection}{\numberline {9.1.2}Water}{16}{subsubsection.9.1.2}%
\contentsline {subsubsection}{\numberline {9.1.3}Moisture}{16}{subsubsection.9.1.3}%
\contentsline {subsubsection}{\numberline {9.1.4}Snow}{16}{subsubsection.9.1.4}%
\contentsline {subsubsection}{\numberline {9.1.5}Red Edge}{17}{subsubsection.9.1.5}%
\contentsline {subsubsection}{\numberline {9.1.6}Normalised Burn Ratio}{17}{subsubsection.9.1.6}%
\contentsline {subsubsection}{\numberline {9.1.7}Green Normalised Difference Vegetation Index}{17}{subsubsection.9.1.7}%
\contentsline {subsection}{\numberline {9.2}Ratio Minus One}{17}{subsection.9.2}%
\contentsline {subsubsection}{\numberline {9.2.1}Red Edge Chlorophyll (RECI)}{17}{subsubsection.9.2.1}%
\contentsline {subsubsection}{\numberline {9.2.2}Green Chlorophyll (GCI)}{17}{subsubsection.9.2.2}%
\contentsline {subsection}{\numberline {9.3}Soil Adjusted Vegetation Indices}{17}{subsection.9.3}%
\contentsline {subsubsection}{\numberline {9.3.1}Soil Adjusted Vegetation (SAVI)}{17}{subsubsection.9.3.1}%
\contentsline {subsubsection}{\numberline {9.3.2}Optimised SAVI (OSAVI)}{18}{subsubsection.9.3.2}%
\contentsline {subsubsection}{\numberline {9.3.3}Modified SAVI (MSAVI)}{18}{subsubsection.9.3.3}%
\contentsline {subsection}{\numberline {9.4}Collected other indices}{18}{subsection.9.4}%
\contentsline {subsubsection}{\numberline {9.4.1}Enhanced Vegetation Index (EVI)}{18}{subsubsection.9.4.1}%
\contentsline {subsubsection}{\numberline {9.4.2}Structure Intensive Pigment Vegetation Index (SIPI)}{18}{subsubsection.9.4.2}%
\contentsline {subsubsection}{\numberline {9.4.3}Atmospherically Resistant Vegetation Index (ARVI)}{18}{subsubsection.9.4.3}%
\contentsline {subsubsection}{\numberline {9.4.4}Visible Atmospherically Resistant Index (VARI)}{18}{subsubsection.9.4.4}%
\contentsline {section}{\numberline {10}Useful Resources}{19}{section.10}%
