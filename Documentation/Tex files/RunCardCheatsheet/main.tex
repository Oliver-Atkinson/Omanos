\documentclass[nofootinbib,a4paper,11pt]{article}
\usepackage{jheppub}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{float}
\usepackage{multicol,multirow}
\usepackage{amsmath}
\usepackage{subcaption}
\usepackage{array}
\usepackage{color}
\usepackage{hyperref}
\hypersetup{colorlinks=true}
\usepackage{longtable}
\usepackage{graphics,graphicx,epsfig,ulem,booktabs}
\setcounter{tocdepth}{2}

\usepackage{listings}


\makeatletter
\newcommand\footnoteref[1]{\protected@xdef\@thefnmark{\ref{#1}}\@footnotemark}
\makeatother

\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}


\definecolor{green}{rgb}{0.1,0.5,0.0}
\newcommand{\blue}{\color{blue}}
\newcommand{\magenta}{\color{magenta}}
\newcommand{\green}{\color{green}}
\newcommand{\red}{\color{red}}
\newcommand{\orange}{\color{orange}}
\newcommand\inputpgf[2]{{
\let\pgfimageWithoutPath\pgfimage
\renewcommand{\pgfimage}[2][]{\pgfimageWithoutPath[##1]{#1/##2}}
\input{#1/#2}
}}

\newcommand{\sat}{satellite }
\newcommand{\sats}{satellites }
\newcommand{\veg}{vegetation }


\title{GEEOAPP Run Card Summary Sheet}


\begin{document}

\noindent\textbf{{\Large \texttt{GEEOAPP} Run Card Summary Sheet}
}
\hrule
\vspace{2em}

\noindent A quick outline of the run card formalism, the various options for each variable, which variables are required and which are optional. Run cards should be stored in the \texttt{RunCard} directory, which is accessed when \texttt{GEEOAPP} is run with the command line call
\begin{lstlisting}[language=bash]
    $ python GEEOAPP.py RunCardName.yml
\end{lstlisting}
More detailed information can be found in the full user manual.
Flags are switched on with a value of 1 and off with 0; those without a given value are assumed to be off.

\vspace{1em}
\noindent\textbf{Names}: Tells \texttt{GEEOAPP} where to save the outputs and what to call them. If folders don't exist they will be created. All names are required, even if they will not be used, e.g. in the case of no masking being applied, there must still be a name for \texttt{MaskSaveDir}.
\begin{itemize}
    \item \texttt{Name}: The prefix for all filenames that will be generated, e.g. \texttt{Name\_Index\_Satellite\_Dates.tif}  
    \item \texttt{ImSaveDir}: The Google Drive folder where the images will be saved
    \item \texttt{MaskSaveDir}: The Google Drive folder where any masks will be saved, if mask saving is switched on
    \item \texttt{PlotDir}: A local folder relative to the package where the plots from any quantitative analyses or custom visualisations will be saved
\end{itemize}

\vspace{1em}
\noindent\textbf{Area of interest}: Defines the area of interest, in terms of the maximum and minimum longitude and latitude. Required.

\vspace{1em}
\noindent\textbf{Satellite}: Specifies the satellite mission from which to request data. Required. The available satellites are detailed in the \texttt{Satellites.yml} file.

\vspace{1em}
\noindent\textbf{Processing}: Information on how image collections should be filtered and reduced to single images. 
\begin{itemize}
    \item \texttt{reducer}: The operation by which to reduce image collections to a single image, currently supports median and mosaic. Required. 
    \item \texttt{cloud\_cover}: Maximum allowed percentage of cloud cover in a single image in the collection. Assumed to be 100 if no input given.
    \item \texttt{cloud\_mask}: Flag for application of the appropriate cloud masking function.
\end{itemize}

\vspace{1em}
\noindent\textbf{Dates}: Gives the time period and the interval to query data collections. Dates should be formatted as `YYYY--MM--dd'.
\begin{itemize}
    \item \texttt{start\_date}: Start of the overall period. Required. 
    \item \texttt{end\_date}: End of the overall period. Required.
    \item \texttt{delta\_period}: Numerical value of the time period to sample. Defaults to 1 if not given.
    \item \texttt{period}: Unit of time for the time period, one of year, month, week or day. Defaults to year if not given.
    \item \texttt{delta\_sub}: Numerical value of the time of the sub period to sample. Acts as the switch to turn on sub period sampling, optional.
    \item \texttt{subperiod}: Unit of time for the time sub period, one of year, month, week or day. Required if sub period sampling is desired.
    \item \texttt{sub\_start}: Start date of the first sub period sample. Defaults to the overall start date if sub period sampling is on and no input is given here.
\end{itemize}



\vspace{1em}
\noindent\textbf{Masking}: Information for mask generation and application.
\begin{itemize}
    \item \texttt{mask\_bandName}: Name of the band from which the mask will be calculated. If no input is given, no mask will be generated.
    \item \texttt{upper\_bound}: Numerical value of upper bound of the mask. Both upper and lower bounds must be given if generating a mask (use a dummy value if only one bound is desired).
    \item \texttt{lower\_bound}: Numerical value of lower bound of the mask. Both upper and lower bounds must be given if generating a mask (use a dummy value if only one bound is desired).
    \item \texttt{save\_raster}: Flag for saving the mask as a raster image (\texttt{.tif})
    \item \texttt{save\_vector}: Flag for saving the mask as a vector image (\texttt{.shp})
    \item \texttt{load\_name}: File path of a Google Earth Engine shapefile asset to load in as a mask. If no file name is given, no mask will be loaded.
    \item \texttt{load\_invert}: Flag for inversion of the loaded mask.
    \item \texttt{load\_overlap}: Length of overlapping geometries to clip from a loaded mask. Can take a value of 1 or 2.
\end{itemize}

\vspace{1em}
\noindent\textbf{Analyses}: Information for analysing the data.
\begin{itemize}
    \item \texttt{indices}: List of indices and RGB composites to calculate and save images of.
    \item \texttt{areas}: List of coordinate lists on which to perform quantitative analyses, where each list can be either a point [x,y] or a geometry [xMin, yMin, xMax, yMax].
    \item \texttt{bandName}: Name of the band on which to perform the quantitative analyses.
    \item \texttt{quant\_analyses}: List of quantitative analyses to be performed.
    \item \texttt{upper\_bound}: Numerical value of the upper bound for a threshold analysis of an area.
    \item \texttt{lower\_bound}: Numerical value of the lower bound for a threshold analysis of an area.
    \item \texttt{save\_flag}: Flag for saving a true colour image with the analysed areas highlighted. 
\end{itemize}

\vspace{1em}
\noindent\textbf{Visualisations}: Entirely optional set of inputs for customising image visualisations. Visualisations can either be defined using the direct input of the first three options, or generated by using some combination of the others. A separate visualisation can be specified for each index. Default visualisations will be used if no information is included here.
\begin{itemize}
    \item \texttt{vis\_min}: Minimum pixel value for the visualisation.
    \item \texttt{vis\_max}: Maximum pixel value for the visualisation.
    \item \texttt{palette}: Either a list of colours (each and RGB list or hex string) or the name of a palette as found in \texttt{Palettes.yml}.
    \item \texttt{make}: Flag for automated generation of a visualisation.
    \item \texttt{floor}: Colour which pixels of value below \texttt{vis\_min} will be displayed as. Defaults to black.
    \item \texttt{ceiling}: Colour which pixels of value above \texttt{vis\_max} will be displayed as. Defaults to white.
    \item \texttt{vals}: The values for the visualisation, as a list of steps in the visualisation from steps case, or list [min, max] in the ramp case.
    \item \texttt{colours}: The colours from which to form a palette, one for each interval in the case of only visualisation from steps, or the colours from which to create a ramp if the \texttt{ramp\_flag} is on.
    \item \texttt{steps\_flag}: Flag for using a series of steps to create the palette.
    \item \texttt{ramp\_flag}: Flag for creating a colour ramp to use a palette.
    \item \texttt{n\_steps}: Number of steps to use when creating a colour ramp. 
    \item \texttt{show}: Flag for saving a plot of the visualisation.
\end{itemize}

\end{document}
