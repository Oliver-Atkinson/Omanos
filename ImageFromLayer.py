#import ee, time
from ee_plugin import Map
from qgis.core import QgsProject

#ee.Authenticate()

# rename this to the layer with your AOI
layer = QgsProject.instance().mapLayersByName("South_Desside_North_Angus_DMG")[0]

# grab that AOI and create an ee polygon
geometry = list(layer.getFeatures())[0].geometry()
box = ee.Geometry.Rectangle([-2.785, 57.08, -3.42, 56.77])

QgsGeometry.convertToSingleType(geometry)

print(geometry)

AOI = []
for item in geometry.asPolygon()[0]:
    AOI.append([item.x(), item.y()])
AOI = ee.Geometry.Polygon(AOI)

##  GET S2 IMAGE
collection = ee.ImageCollection("COPERNICUS/S2_SR").filterDate('2021-01-01','2021-01-29').filterBounds(AOI).sort("DATE_ACQUIRED").limit(5)

image = ee.Image(collection.first())
print(1)

#index = image.normalizedDifference(['B8A','B11'])  #doesn't work due to QGIS plugin bug, use the following instead

Band8A = collection.median().select('B8A')
Band11 = collection.median().select('B11')

subtract = Band8A.subtract(Band11)
add = Band8A.add(Band11)
index = subtract.divide(add)

index = index.clip(AOI)

vis_index = {'min': -1, 'max': 1, 'palette': ['red', 'white', 'blue']}

print(2)
#Map.addLayer(image, vis, 'test_S2')
Map.addLayer(index, vis_index, 'index_S2')
